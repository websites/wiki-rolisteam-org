Title: Images
Date: 2017-06-11 10:20
slug: images
status: hidden
lang: fr


Des images peuvent être partagée grâce à **Rolisteam**.
Vous pouvez ainsi montrer à vos joueurs des personnages, des lieux ou encore des paysages.

Les images ne peuvent pas être modifiées. Dans le sens qu\`%il est impossible d'écrire par dessus ou dessiner.
Les [cartes]({filename}16_maps.md) ou [carte vectorielle]({filename}17_VectorialMap.md) sont la bonne alternative si vous cherchez cette fonctionnalité.

## Identifier les images parmis les autres types de fenêtre.

All image subwindows have the same icon:  
![image]({static}/images/pictos/image.png)

All image subwindows title is ended by the text: `(image)`.

## Supported formats

**Rolisteam** accepts many standard formats such as: `*.jpg`,`*.png` and `*.bmp`.

It may supports many other formats depending of the platform. The whole list is available in the [diagnostic tab]({filename}22_preferences.md) in the preferences panel.

## Open image

**Rolisteam** can not create new images. They must be opened (`File > Open Image`).
Then, the opened image is sent to all users.

## Save Image

A image can only be saved as part of a scenario (`File` menu then `Save scenario`).

# Contextual Menu

![images]({static}/images/panel/menu_image1.jpg)

By default, `fit window` is enabled. In this configuration, **Rolisteam** ensure the whole image is visible.
It is really useful for huge image.
If you want to zoom in a particular part of the picture, you must disable it.

## Zoom in/out

Sometime it's useful to manage zoom level manually. To get access to zoom level feature, just show the contextual menu and click on `Fit window` to unselect this mode.

You can activate all these feature throught the contextual menu:

![images]({static}/images/panel/menu_image_2.jpg)

-   zoom up, zoom down (the minimun value is 0.2)
-   Fit the image window in the workspace
-   Fit the image to its window (keeping the ratio) (Default)
-   Level of Zoom: Little, normal, Big

## Load Image From Internet

You may load images from the Internet.
The dedicated dialog box is reachable at `file > Open > Online Image`.  

The following dialog appears:

## Step 1
  ![image]({static}/images/en/oponline_image.jpg)

  When the Url is set, **Rolisteam** downloads automatically the image and displays it in the preview panel. It may be required to click on `download` to make **Rolisteam** get the image faster.

## Step 2
![image]({static}/images/tuto/07_load_image_from_internet2_en.jpg)

In this state, it is possible to change the image's title. When it is done. You can click on `Ok`
Then, the image will appears as a regular image.

## Step 3
![image]({static}/images/en/show_image.jpg)



## Hide Image

There are two ways to hide the image. You may click on the cross on the subwindows or you may click on image's name in the `subwindows` menu.
The image is hidden. To show it again, just click on its name again in the `subwindows` menu.

Either ways, the image is hidden only for you. All other players may still see the image.

## Close Image

The **GM** or the owner of the image may close it.
This means the image will disappear from every player's workspace.
To do so, Inside the `file` menu, click on `Close Map/image`


=== END


Les images permettent à l'ensemble des utilisateurs de montrer des dessins ou des photos aux autres joueurs.  

Cette fonction peut également être remplie par un plan, toutefois l'image utilise beaucoup moins de mémoire.  

Si votre but est simplement d'afficher un fichier image, sans y ajouter de modifications, ni d'annotations, utilisez une image, dans le cas contraire utilisez un plan.
Les images sont identifiables dans l'espace de travail par leur icône ainsi que par l'annotation **(image)** dans leur titre.  
A la différence des plans, les images peuvent être ouvertes par tous les utilisateurs.
Pour cela il suffit de cliquer sur `Ouvrir image` dans le menu `Fichier` et de sélectionnerle fichier image (au format JPEG, PNG ou BMP);
celui-ci est alors transféré à l'ensemble des utilisateurs.  
La fermeture de la fenêtre image ne fait que masquer cette dernière, qui peut à nouveau être ouverte en cliquant sur son nom dans le menu `Fenêtre`.

Pour fermer définitivement une image (et pour qu'elle le soit chez tous
les utilisateurs), son propriétaire (celui qui l'a ouverte) ou le MJ
doit cliquer sur `Fermer plan/image` dans le menu `Fichier`.

Une image ne peut être sauvegardée que dans le cadre d'un scénario
(`Sauvegarder scénario` dans le menu `Fichier`).

v 1.6.1
-------

Par défaut, une image s'ouvre dans le mode: Adaptation à sa fenêtre. Il
faut désactiver ce mode (par un click droit) pour avoir accès aux
fonctionnalités plus fines sur le zoom.

Il est possible avec un click droit d'activer des options sur l'image:

-   Zoom (La valeur minimale de zoom est 0.2)
-   Faire rentrer l'image dans l'espace de travail (utile pour les
    très grandes images)
-   Adapter l'image à la fenêtre (garde les proportions ) (Défaut)
-   Palier de Zoom: petit, grand, normal.
-   Des racourcis claviers sont également présents changer le zoom de la
    photo.

v 1.7.0
-------

Il est maintenant possible de charger des images depuis internet. Vous
copiez collez le chemin de l'image dans la boite de dialogue à cette
effet. Cela vous proposer une visualisation et vous pouvez valider.

<p style="text-align: left; width:49%; display: inline-block;"><a href="/fr/music.html">Précédent</a></p>
<p style="text-align: right; width:50%;  display: inline-block;"><a href="/fr/map.html">Suivant</a></p>
